<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    Use SoftDeletes;
    //$category->products
    public function products(){
        return $this->hasMany(Product::class);

    }
}
