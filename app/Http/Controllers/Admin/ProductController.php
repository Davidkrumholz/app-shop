<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller {
    public function index(){
        $products = product::paginate(10);
        return view('admin.products.index')->with(compact('products')); // listado de productos
    }

    public function create(){
        return view('admin.products.create'); //formulario de registro
    }

    public function store(Request $request){
        //Mensajes de error
        $messagesError = [
        'name.required'=>'El campo nombre es obligatorio',
        'name.min'=>'El nombre del producyo debe de tener al menos 3 caracteres',
        'description.required'=>'El campo descripcion corta es obligatorio',
        'description.max'=>'La descricpion debe de tener como maximo 200 caracteres',
        'price.required'=>'El campo precio es obligatorio',
        'price.numeric'=>'El campo debe de ser numerico',
        'price.min'=>'El campo de precio no puede ser negativo'];
        //validacion de datos
        $rules = [
            'name' => 'required|min:3',
            'description' => 'required|max:200', //puede tener un maximo de 200 caracteres
            'price' => 'required|numeric|min:0'

        ];
        $this->validate($request, $rules, $messagesError);
        // registrar nuevos productos en la base de datos
        $product = new Product();
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');
        $product->long_description = $request->input('long_description');
        $product->save();//Hace un insert a la tabla de productos

        return redirect('/admin/products'); 
    }

    public function edit($id){
        //return "Mostrar aqui el form de edicion para el producto con el id $id";
        $product = Product::find($id);
        return view('admin.products.edit')->with(compact('product')); //formulario de edicion de productos

    }

    public function update(Request $request, $id){
        $messagesError = [
            'name.required'=>'El campo nombre es obligatorio',
            'name.min'=>'El nombre del producyo debe de tener al menos 3 caracteres',
            'description.required'=>'El campo descripcion corta es obligatorio',
            'description.max'=>'La descricpion debe de tener como maximo 200 caracteres',
            'price.required'=>'El campo precio es obligatorio',
            'price.numeric'=>'El campo debe de ser numerico',
            'price.min'=>'El campo de precio no puede ser negativo'];
            //validacion de datos
            $rules = [
                'name' => 'required|min:3',
                'description' => 'required|max:200', //puede tener un maximo de 200 caracteres
                'price' => 'required|numeric|min:0'
    
            ];
            $this->validate($request, $rules, $messagesError);

        // registrar nuevos productos en la base de datos
        $product = Product::find($id);
        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');
        $product->long_description = $request->input('long_description');
        $product->save();//Hace un update a la tabla de productos

        return redirect('/admin/products'); 
    }
//El metodo Request es para pasar datos por el metodo POST
    public function destroy($id){
        //elimina producto de la base de datos
        $product = Product::find($id);
        $product->delete();
        return back(); // Regresa a la pantalla donde se encuentra
    }

}