<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use SoftDeletes;
    //$productimage->product
    public function product(){
        return $this->belongsTo(Product::class);
    }

    //acceso al product image
    public function getUrlAttribute(){
        if(substr($this->image, 0, 4) === "http"){
            return $this->image;
        }
        return '/images/products/'. $this->image;
    }
}
