<?php

Route::get('/', 'TestController@welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products/{id}', 'ProductController@show'); // mostrar los productos sin ser administrador
Route::post('/cart', 'CartDetailController@store');

Route::middleware(['auth','admin'])->prefix('admin')->namespace('Admin')->group(function () {   
    Route::get('/products', 'ProductController@index'); //listado de productos
    Route::get('/products/create', 'ProductController@create'); //creacion de productos
    Route::post('/products', 'ProductController@store'); //Se manda informacion del producto a la base de datos registro
    Route::get('/products/{id}/edit', 'ProductController@edit'); // formulario edición
    Route::post('/products/{id}/edit', 'ProductController@update'); // actualizar
    Route::delete('/products/{id}', 'ProductController@destroy'); // formulario eliminar  
    
    Route::get('/products/{id}/images', 'ImageController@index');
    Route::post('/products/{id}/images','ImageController@store');
    Route::delete('/products/{id}/images', 'ImageController@destroy'); // formulario eliminar  
    Route::get('/products/{id}/images/select/{image}', 'ImageController@select'); //destacar una imagen
    //put patch delete
    });
