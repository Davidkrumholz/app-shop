<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'David', 
            'email' => 'davidkrumholz@yahoo.com.mx',
            'password' => bcrypt('sclar123'),
            'admin' => true
        ]);
    }
}
